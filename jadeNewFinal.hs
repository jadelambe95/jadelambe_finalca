import qualified Data.List
import Control.Monad
import Text.Read

-- Adds 2 numbers 
addNums :: (Num a) => a -> a -> a
addNums x y = (x+y)

-- Subtracts 2 numbers 
subNums :: (Num a) => a -> a -> a
subNums x y = (x-y)

-- Multiplies 2 numbers 
multNums :: (Num a) => a -> a -> a
multNums x y = (x*y)

-- Divides 2 numbers 
divNums :: (Fractional a) => a -> a -> a
divNums x y = x / y

-- TAkes in 3 arguments (a, b, c) -> ax^2 + bx + c = 0
solveQuadEqu :: (RealFloat a) => (a, a, a) -> (a, a)
solveQuadEqu (a, b, c) = (x, y) 
   where
      f = (sqrt(b^2 - (4 * a * c)))
      x = (-b + f)/2*a
      y = (-b - f)/2*a
	  
-- Gets product of factorials of n
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial(n-1)

-- Check whether a number is prime
isPrime :: Integer -> Bool
isPrime n = checkPrime n (n-1)
    where
        checkPrime n x
            |x <= 1 = True
            |n `mod` x == 0 = False
            |otherwise = checkPrime n (x-1)
			
-- List all prime numbers 
primes :: [Integer]
primes = filter (isPrime) [1..]

-- Is number a divisible by n (ie 10 is div by 5, 10 goes first)
isDivisible :: (Integral n) => n -> n -> Bool
isDivisible a n 
    |a `mod` n /= 0 = False
	|otherwise = True
	
-- Gets divisors of a number
divisors :: Int -> [Int]
divisors n = [x | x <- [1..n], n `rem` x == 0]

-- Multiply 2x2 Matrix, take 1st list as top row of matrix
-- and 2nd list as 2nd row of matrix
matrixMult :: (Num a) => [[a]] -> [[a]] -> [[a]] 
matrixMult a b = [ [ sum $ zipWith (*) ar bc | bc <- (Data.List.transpose b) ] | ar <- a ]

main = do
   putStrLn "Welcome to the Second Level Maths Library!"
   putStrLn "1. Add 2 numbers"
   putStrLn "2. Quadratic Equation Solver"
   putStrLn "3. Factorial Generator"
   putStrLn "4. Prime Numbers"
   putStrLn "5. Divisors"
   putStrLn "6. Matrices"
   putStrLn "Enter the number of which section you want to choose:"
   input <- getLine
   putStrLn $ "Number " ++ input ++ " selected!"
   --when	(input == "1") $ do
   --   putStrLn "Enter the first number"
   --   num1String <- getLine 
   --   let num1 = read num1String
   --   putStrLn "Enter the second number"
   --   num2String <- getLine
    --  let num2 = read num2String
    --  addNums num1 num2
	--  main

--addUtil :: (Num a) => a -> a -> a
   
