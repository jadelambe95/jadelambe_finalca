import Data.Map --Map
import Data.List
import Data.Ord
import qualified Data.Map as B
import Data.Maybe

--Board Size
b :: Int
b = 7

--Players
data Player = X | O
   deriving (Bounded, Enum, Eq, Ord, Show)
   
--Define what's in a space
showSpace :: Maybe Player -> Char
showSpace Nothing  = ' '
showSpace (Just X) = 'X'
showSpace (Just O) = 'O'
--type spacing = Maybe Player
--type position = (Int, Int)

--Show Board
--instance Show Board where
  --  show (Board x) = concatMap row [7,4,1] ++ sep where
    --    sep = "+---+---+---+\n"
      --  row n = sep ++ concatMap col [0,1,2] ++ "|\n" where
        --    col m = "| " ++ show (x ! (n+m)) ++ " "
--http://www.gamedev.net/topic/527050-my-first-haskell-game/

--type sBoard = [Char]
--showBoard :: sBoard -> String
--showBoard str =
--  "   |   |   \n" ++
--  " " ++ str !! 0 : [] ++ " | " ++ str !! 1 : [] ++ " | " ++ str !! 2 : []  ++ " | " ++ str !! 3 : [] ++ " | " ++ str !! 4 : [] ++ " | " ++ str !! 5 : [] ++ " | " ++ str !! 6 : [] ++ " \n" ++
--  "   |   |  \n" ++
--  "---+---+---\n" ++
--  "   |   |   \n" ++
--  " " ++ str !! 7 : [] ++ " | " ++ str !! 8 : [] ++ " | " ++ str !! 9 : []  ++ " | " ++ str !! 10 : [] ++ " | " ++ str !! 11 : [] ++ " | " ++ str !! 12 : [] ++ " | " ++ str !! 13 : [] ++ " \n" ++
--  "   |   |   \n" ++
--  "---+---+---\n" ++
--  "   |   |   \n" ++
--  " " ++ str !! 14 : [] ++ " | " ++ str !! 15 : [] ++ " | " ++ str !! 16 : []  ++ " | " ++ str !! 17 : [] ++ " | " ++ str !! 18 : [] ++ " | " ++ str !! 19 : [] ++ " | " ++ str !! 20 : [] ++ " \n" ++
--  "   |   |   \n"
 
instance Show Board where
    show field@(Board rows columns _) = unlines $
        [ concat [show i | i <- [0 .. columns - 1]]
        ] ++
        [ [showSpace (getPos row column field) | column <- [0 .. columns - 1]]
        | row <- [0 .. rows - 1]
        ]
--                                 FIND LINK!!!!!!!!!!!!!!!!!!!!!!!

--Set up the playing field
data Board = Board
   {boardRows :: Int,
    boardColumns :: Int,
    boardSpaces :: Map (Int, Int) Player
   }

-- Set board as empty
setBoardEmpty :: Int -> Int -> Board
setBoardEmpty rows columns = Board rows columns B.empty
   
--Get where things are
getPos :: Int -> Int -> Board -> Maybe Player
getPos row column = B.lookup (row, column) . boardSpaces

--playerMove :: Int -> Char -> Board -> Board
--playerMove column showSpace Board(rows columns spaces)
--   |column < 0 column >= columns = field

--move :: Board -> Char -> Int -> Board
--move (b:board) ch pos
--  | pos > 0 = b:[] ++ (move board ch (pos - 1))
--  | otherwise = ch:[] ++ board
--https://systematicgaming.wordpress.com/code-haskell-tic-tac-toe/ TICTACTOE EX
  
main :: IO ()
main = do
    putStrLn "CONNECT 4\n"
	--putStr $ show Board
    --putStrLn $ show Board


--Cycle ties a finite list into a circular one, or equivalently, the infinite repetition of the original list. 
--It is the identity on infinite lists